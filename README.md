Run with docker:
- `docker-compose run web rake db:create` to create the databases
- `docker-compose up --build` to build the container and run
- Access the website at http://localhost:3000
